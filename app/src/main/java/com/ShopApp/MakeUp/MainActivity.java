package com.ShopApp.MakeUp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.snackbar.Snackbar;
import com.ShopApp.MakeUp.R;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout rootlayout;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run()
        {

            if (!isNetworkAvailable()) {
                Snackbar snackbar = Snackbar.make(rootlayout, "اتصال به اینترنت قطع است",100000);
                snackbar.setAction("امتحان کنید", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                });
                snackbar.setActionTextColor(Color.BLUE);

                snackbar.show();
            }
            else
            {
                startActivity(new Intent(MainActivity.this, Main_page.class));
                finish();
            }


            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the shared preferences
        SharedPreferences preferences = getSharedPreferences("my_preferences", MODE_PRIVATE);

        // Check if onboarding_complete is false
        if(!preferences.getBoolean("onboarding_complete",false))

        { // Start the onboarding Activity
            Intent onboarding = new Intent(this, com.ShopApp.MakeUp.OnBoarding.class);
            startActivity(onboarding);

            // Close the main Activity
            finish();
            return;
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        rootlayout = findViewById(R.id.root_layout_splash);

        handler.postDelayed(runnable,3000);


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {

    }


    private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

    }
