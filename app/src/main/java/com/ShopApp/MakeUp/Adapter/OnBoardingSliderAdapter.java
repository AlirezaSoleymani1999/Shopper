package com.ShopApp.MakeUp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ShopApp.MakeUp.R;

public class OnBoardingSliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    Typeface typeface;

    public OnBoardingSliderAdapter(Context context, Typeface typeface) {
        this.context = context;
        this.typeface = typeface;
    }

    public int[] images ={
                            R.drawable.online_shopping,
                            R.drawable.post_ad,
                            R.drawable.payment_method
                            };
    public String[] description ={
                            "از جدید ترین محصولات دیدن کنید و از خرید لذت ببرید",
                            "تحویل پستی محصولات به سراسر ایران طی 24 تا 72 ساعت",
                            "پرداخت هزینه از طریق کلیه کارت های عضو شتاب"
                            };

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide_layout,container,false);

        ImageView imageView = view.findViewById(R.id.image);
        TextView textView = view.findViewById(R.id.description);

        textView.setTypeface(typeface);
        imageView.setImageResource(images[position]);
        textView.setText(description[position]);

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((RelativeLayout)object);
    }
}
