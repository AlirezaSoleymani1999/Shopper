package com.ShopApp.MakeUp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ShopApp.MakeUp.Model.Product;
import com.ShopApp.MakeUp.R;

import java.util.ArrayList;

 public class RecyclerViewAdapterWithOffPrice extends RecyclerView.Adapter<RecyclerViewAdapterWithOffPrice.MyViewHolder> {

    private ArrayList<Product> productsModel;

    Context c;
    public RecyclerViewAdapterWithOffPrice(Context c, ArrayList<Product> productsModel) {
        this.productsModel = productsModel;
        this.c = c;
    }

     private void overrideFonts(final Context context, final View v) {
         try {
             if (v instanceof ViewGroup) {
                 ViewGroup vg = (ViewGroup) v;
                 for (int i = 0; i < vg.getChildCount(); i++) {
                     View child = vg.getChildAt(i);
                     overrideFonts(context, child);
                 }
             } else if (v instanceof TextView ) {
                 ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN Sans Bold.ttf"));
             }
         } catch (Exception e) {
         }
     }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_layout_withoffprice,null);


        overrideFonts(c,v);

        MyViewHolder mvh = new MyViewHolder(v);

        return mvh;

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i)
    {


        if (productsModel.get(i).getName().length() > 55 )
        {
            String dec2=productsModel.get(i).getName();

            dec2 = dec2.substring(0,48) + "...";
            myViewHolder.Description.setText(dec2);

        }else
            {
                myViewHolder.Description.setText(productsModel.get(i).getName());

            }


        myViewHolder.imageView.setImageResource(productsModel.get(i).getImage());
        myViewHolder.Price.setText(productsModel.get(i).getPrice());
        myViewHolder.OffPrice.setText(productsModel.get(i).getMenuId());

        myViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Toast.makeText(c," "+i,Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return productsModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        TextView Description,Price,OffPrice;
        ImageView imageView;

        MyViewHolder(View itemView) {
            super(itemView);
            Description = itemView.findViewById(R.id.RecyclerView_Description);
            Price = itemView.findViewById(R.id.RecyclerView_Price);
            OffPrice = itemView.findViewById(R.id.RecyclerView_OffPrice);
            imageView = itemView.findViewById(R.id.RecyclerView_ImageView);

        }
    }


}
