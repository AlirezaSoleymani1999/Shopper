package com.ShopApp.MakeUp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ShopApp.MakeUp.Adapter.OnBoardingSliderAdapter;

public class OnBoarding extends AppCompatActivity {

    private ViewPager pager;
    private Button pre;
    private Button next;
    TextView tv;
    Typeface typeface;
    private OnBoardingSliderAdapter adapter;
    private TextView[] dots;
    LinearLayout dotsLayout;
    int currentPage=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

         typeface = Typeface.createFromAsset(getAssets(),"fonts/IRAN Sans Regular.ttf");
         dotsLayout = findViewById(R.id.dots);
         adapter = new OnBoardingSliderAdapter(this,typeface);
         pager = findViewById(R.id.pager);

         pager.setAdapter(adapter);
         addDotIndecator(0);
         pre = findViewById(R.id.pre);
         next = findViewById(R.id.next);

         next.setTypeface(typeface);
         pre.setTypeface(typeface);

         next.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 if(currentPage == 2)
                 {
                     SharedPreferences preferences = getSharedPreferences("my_preferences", MODE_PRIVATE);
                     preferences.edit().putBoolean("onboarding_complete",true).apply();

                     Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                     startActivity(intent);
                     finish();
                 }
                 pager.setCurrentItem(currentPage +1);
             }
         });

         pre.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 pager.setCurrentItem(currentPage-1);
             }
         });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                addDotIndecator(i);
                currentPage=i;

                if(i==0)
                {
                    next.setEnabled(true);
                    pre.setEnabled(false);
                    pre.setVisibility(View.INVISIBLE);
                    next.setText("بعدی");
                }
                else if (i == dots.length -1)
                {
                    next.setEnabled(true);
                    pre.setEnabled(true);
                    pre.setText("قبلی");
                    pre.setVisibility(View.VISIBLE);
                    next.setText("خب");

                }
                else
                {
                    next.setEnabled(true);
                    pre.setEnabled(true);
                    pre.setText("قبلی");
                    pre.setVisibility(View.VISIBLE);
                    next.setText("بعدی");
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }


    public void addDotIndecator(int position)
    {
        dots = new TextView[3];
        dotsLayout.removeAllViews();

        for(int i =0; i < dots.length;i++)
        {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dotsColor));
            dotsLayout.addView(dots[i]);
        }

        if(dots.length >0)
        {
             dots[position].setTextColor(getResources().getColor(R.color.white));
        }
    }


}
