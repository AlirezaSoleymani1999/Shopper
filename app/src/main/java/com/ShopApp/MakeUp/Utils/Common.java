package com.ShopApp.MakeUp.Utils;
import com.ShopApp.MakeUp.Model.User;
import com.ShopApp.MakeUp.Retrofit.IShopperAPI;
import com.ShopApp.MakeUp.Retrofit.RetrofitClient;
import com.ShopApp.MakeUp.Retrofit.RetrofitScalarsClient;

public class Common {

    public static final String BASE_URL="http://alirezasoleymani.ir/";

    public static User currentUser=null;

    //database
    //public static MITIRoomDatabase mitiRoomDatabase;
    //public static CartRepository cartRepository;
    //public static FavoriteRepository favoriteRepository;



    public static IShopperAPI getAPI(){
        return RetrofitClient.getClient(BASE_URL).create(IShopperAPI.class);
    }
    public static IShopperAPI getScalarsAPI(){
        return RetrofitScalarsClient.getScalarsClient(BASE_URL).create(IShopperAPI.class);
    }

}
