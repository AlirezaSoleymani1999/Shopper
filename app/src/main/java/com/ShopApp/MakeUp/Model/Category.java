package com.ShopApp.MakeUp.Model;

public class Category {


    public Category() {
    }
    private String ID;

    public String getID() { return this.ID; }

    public void setID(String ID) { this.ID = ID; }

    private String Name;

    public String getName() { return this.Name; }

    public void setName(String Name) { this.Name = Name; }

    private String Link;

    public String getLink() { return this.Link; }

    public void setLink(String Link) { this.Link = Link; }
}
