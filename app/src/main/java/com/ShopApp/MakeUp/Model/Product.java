package com.ShopApp.MakeUp.Model;

public class Product {
    public String ID ;
    public String Name ;
    public String Link ;
    public String Price;
    public String MenuId ;
    public int image;


    public Product(String name, String price, String menuId,int image) {
        this.Name = name;
       this.Price = price;
       this.MenuId = menuId;
       this.image=image;

    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getImage(){return image;}

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }
}
