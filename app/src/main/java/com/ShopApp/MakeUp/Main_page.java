package com.ShopApp.MakeUp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ShopApp.MakeUp.Adapter.RecyclerViewAdapter;
import com.ShopApp.MakeUp.Adapter.RecyclerViewAdapterWithOffPrice;
import com.ShopApp.MakeUp.Model.Product;
import com.ShopApp.MakeUp.Utils.CustomTypefaceSpan;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cn.iwgang.countdownview.CountdownView;


public class Main_page extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ArrayList<Product> productModel;

    RecyclerView OffRecyclerView;
    RecyclerView MosetSellerRecyclerView;

    NavigationView navView;

    Button btn_dialog;
    Button category;

    LinearLayout container;

    CountdownView countdownView;

    Toolbar toolbar;

    Context context = this;
    Handler handler = new Handler();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        checking();


        category = findViewById(R.id.category);
        navView = findViewById(R.id.nav_view);
        container = findViewById(R.id.container);

        countdownView = findViewById(R.id.CountDownView);
        countdownView.start(432000000); // Millisecond

        overrideFonts(getApplicationContext(),navView.getHeaderView(0));
        overrideFonts(getApplicationContext(),container);

        Menu m = navView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

       // category.setTypeface(typeface);
         toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent goCategoty = new Intent(getApplicationContext(), Category.class);
                startActivity(goCategoty);

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);

        productModel = new ArrayList<>();

        productModel.add(new Product("کیره خر", "200,22", "150,00", R.drawable.payment_method));
        productModel.add(new Product("کیره بز", "300,22", "160,00", R.drawable.payment_method));
        productModel.add(new Product("تخم سگ", "500,22", "60,00", R.drawable.payment_method));
        productModel.add(new Product("تخم جن", "200,22", "150,00", R.drawable.payment_method));
        productModel.add(new Product("کوون حاج علی معجر", "50,22", "30,00", R.drawable.payment_method));
        productModel.add(new Product("علیرضا خمسه تو کونت مهدی", "200,22", "150,00", R.drawable.payment_method));
        productModel.add(new Product("کرم نیوا مناسب برای دست و صورت در انداره های مختلف مناسب برای مناقط حساس بدن مصل کیر و خایه", "200,22", "150,00", R.drawable.payment_method));

        OffRecyclerView = findViewById(R.id.OffRecyclerView);
        MosetSellerRecyclerView = findViewById(R.id.MostSellerRecyclerView);


        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager llm2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);


        OffRecyclerView.setLayoutManager(llm);
        MosetSellerRecyclerView.setLayoutManager(llm2);

        RecyclerViewAdapterWithOffPrice ma = new RecyclerViewAdapterWithOffPrice(this, productModel);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(this,productModel);

        OffRecyclerView.setAdapter(ma);
        MosetSellerRecyclerView.setAdapter(recyclerViewAdapter);

    }


    //Exit app when click back
    boolean isBackButtonClicked = false;


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isBackButtonClicked) {
            super.onBackPressed();
            return;
        }

        this.isBackButtonClicked = true;
        Toast.makeText(this, "برای خروج دوباره امتحان کنید", Toast.LENGTH_SHORT).show();


    }

    RelativeLayout relativeLayout;
    View view;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main_page,menu);

       menu.findItem(R.id.action_cart).setActionView(R.layout.action_bar_noticication_icon);
       view = getLayoutInflater().inflate(R.layout.action_bar_noticication_icon,toolbar);
       relativeLayout = view.findViewById(R.id.relative);

        return true;
    }

    int i=1;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search)
        {


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN Sans Regular.ttf"));
            }
        } catch (Exception e) {
        }
    }

    private void checking() {
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if (!isNetworkAvailable()) {
                            View view = LayoutInflater.from(context).inflate(R.layout.cs_lay, null, false);
                            btn_dialog = view.findViewById(R.id.btn_dialog_internet);

                            final Dialog dialog = new Dialog(context);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(view);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            dialog.setCancelable(false);
                            overrideFonts(getApplicationContext(),view);
                            dialog.show();
                            t.cancel();
                            btn_dialog.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (!isNetworkAvailable())
                                        Toast.makeText(Main_page.this, "اینترنت فعال نیست", Toast.LENGTH_SHORT).show();
                                    else {
                                        dialog.cancel();
                                        checking();
                                    }
                                }
                            });
                        }

                    }
                });
            }
        }, 0, 500);
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/IRAN Sans Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

}
